import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class convers
{
    BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
  
    void menu()throws IOException
    {
        while(true)
        {
       
        System.out.println("1.-BINARIO => HEXADECIMAL");
        System.out.println("2.-HEXADECIMAL => BINARIO");
        System.out.print("3.-SALIR\n OPCION: ");
      
        int opc=Integer.parseInt(in.readLine());
        String valor;
        System.out.print("\n");
      
            switch(opc)
            {
                case 1:
                    System.out.println("\n\nBINARIO => HEXADECIMAL");
                    System.out.print("nBINARIO= ");
                    valor=in.readLine();
                    bin_hex(valor);
                    break;
                case 2:
                    System.out.println("\n\nHEXADECIMAL => DECIMAL");
                    System.out.print("nHEXADECIMAL= ");
                    valor=in.readLine();
                    hex_bin(valor);
                    break;
                case 13:
                    System.exit(0);
                    break;
            }
        }
    }
  
  
   
    void bin_hex(String valor)
    {
        int dec=Integer.parseInt(valor,2);
        valor=Integer.toHexString(dec);
        System.out.print("HEXADECIMAL: "+valor);
    }
  
  
  
    void hex_bin(String valor)
    {
        int dec=Integer.parseInt(valor,16);
        valor=Integer.toBinaryString(dec);
        System.out.print("BINARIO: "+valor);
    }
  
  
  
  
    
  
    public static void main(String [] args) throws IOException
    {
        convers conversion=new convers();
        conversion.menu();
    }
}